# Checkov IaC SAST CI Component Example

## Migrated to a CI/CD Component

This CI/CD Extension was migrated to a GitLab CI/CD Component which can be found at https://gitlab.com/explore/catalog/guided-explorations/ci-components/checkov-iac-sast

This project is now just the working example of that component as can be seen by the component include in [.gitlab-ci.yml](.gitlab-ci.yml)
